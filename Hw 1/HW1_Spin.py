

'''
@file HW1_Spin.py


This file houses code to fufill HW 1 deliverables. It involves an elevator moving between two floors

The user has two buttons, button_1 and button_2 correlating to floor one and floor two respectivey

There are also sensors at each floor to determine when the elevator should stop

Source code is available at  https://bitbucket.org/spinb17/mechatronicslab/src/master/Hw%201/HW1_Spin.py 

@author Ben Spin

@date October 6,2020
'''

from random import choice

import time

class TaskElevator:
    '''
    @brief      A finite state machine to control and elevator.
    @details    This class implements a finite state machine to control the
                operation of an elevator.
    '''
    
    ## Constant defining State 0 - Initialization
    S0_INIT = 0    
    
    ## Constant defining State 1
    S1_MOVING_DOWN  = 1    
    
    ## Constant defining State 2
    S2_MOVING_UP = 2    
    
    ## Constant defining State 3
    S3_STOPPED_FLOOR_1      = 3    
    
    ## Constant defining State 4
    S4_STOPPED_FLOOR_2     = 4
  
    
    def __init__(self, interval,TaskName, Motor, Button_1, Button_2, First, Seccond):
        
        '''
        @brief            Creates a TaskElevator object.
        
        @param Motor      An object from class Motor representing elevator motor
        @param Button_1  An object from class Button representing the first floor button
        @param RightLimit An object from class Button representing the seccond floor button
        @param Motor      An object from class MotorDriver representing a DC motor.
     
        '''
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## Class for the motor
        self.Motor = Motor #Stores motor for later use
        
        ## Class for first floor button (button_1)
        self.Button_1 = Button_1
        
        ## Class for the seccond floor button (button_2)
        self.Button_2 = Button_2
        
        ## Class for lower floor sensor
        self.First = First
        
        ## Class for upper floor sensor
        self.Seccond = Seccond
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ##  The amount of time in seconds between runs of the task
        self.interval = interval
        
        ## Identifying wich task is running
        self.TaskName = TaskName
        
        ## The timestamp for the first iteration
        self.start_time = time.time()
        
        ## The "timestamp" for when the task should run next
        self.next_time = self.start_time + self.interval
        
    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''
        self.curr_time = time.time()
        if self.curr_time > self.next_time: #setting current time
            
        #checking our time stamp
            if(self.state == self.S0_INIT):
                
                print(str(self.runs) + ': State 0 ' + str(self.curr_time-self.start_time))
                
                # Run State 0 Code
                if (self.Seccond.getButtonState()):
                    self.transitionTo(self.S4_STOPPED_FLOOR_2)
                    
                    
                elif (self.First.getButtonState()):
                    self.transitionTo(self.S3_STOPPED_FLOOR_1)
                    
               
               
            
            elif(self.state == self.S1_MOVING_DOWN):
                print(str(self.runs) + ': State 1 ' + str(self.curr_time-self.start_time))
                # Run State 1 Code
                 
                 
                
                if (self.First.getButtonState()):
                    self.transitionTo(self.S3_STOPPED_FLOOR_1)
                    self.Motor.Stopped()
                    
                    
            
            elif(self.state == self.S2_MOVING_UP):
                print(str(self.runs) + ': State 2 ' + str(self.curr_time-self.start_time))
                # Run State 2 Code
                
                if (self.Seccond.getButtonState()):
                   self.transitionTo(self.S4_STOPPED_FLOOR_2)
                   self.Motor.Stopped()
                   
                
            
            elif(self.state == self.S3_STOPPED_FLOOR_1):
                print(str(self.runs) + ': State 3 ' + str(self.curr_time-self.start_time))
                # Run State 3 Code
                
                if(self.Button_2.getButtonState()):
                    self.transitionTo(self.S2_MOVING_UP)
                    self.Motor.Up()
                
            elif(self.state == self.S4_STOPPED_FLOOR_2):
                print(str(self.runs) + ': State 4 ' + str(self.curr_time-self.start_time))
                # Run State 4 Code
                
                if(self.Button_1.getButtonState()):
                    self.transitionTo(self.S1_MOVING_DOWN)
                    self.Motor.Down()
                
          
            
            else:
                # Invalid state code (error handling)
                pass
            
            self.runs += 1
            
            # Specifying the next time the task will run
            self.next_time += self.interval
    
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
       
        self.state = newState
        print (self.TaskName + ' to state '+str(self.state)) 
    
        

class Button:
    '''
    @brief      A pushbutton class
    @details    This class represents a button that the can be pushed by the
                imaginary driver to choose a floor. As of right now
                this class is implemented using "pseudo-hardware". That is, we
                are not working with real hardware IO yet, this is all pretend.
    '''
    
    def __init__(self, pin):
        '''
        @brief      Creates a Button object
        @param pin  A pin object that the button is connected to
        '''
        
        ## The pin object used to read the Button state
        self.pin = pin
        
        print('Button object created attached to pin '+ str(self.pin))

    
    def getButtonState(self):
        '''
        @brief      Gets the button state.
        @details    Since there is no hardware attached this method
                    returns a randomized True or False value.
        @return     A boolean representing the state of the button.
        '''
        return choice([True, False])

class MotorDriver:
    '''
    @brief      A motor driver.
    @details    This class represents a motor driver used to lift the elevator up and down
    '''
    
    def __init__(self):
        '''
        @brief Creates a MotorDriver Object
        '''
        pass
    
    def Up(self):
        '''
        @brief Moves the elevator up
        '''
        print('Elevator going up...')
    
    def Down(self):
        '''
        @brief Moves the elevator down
        '''
        print('Elevator going down...')
    
    def Stopped(self):
        '''
        @brief Elevator stops moving
        '''
        print('Youve reached your floor...')



























