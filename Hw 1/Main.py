'''
@file Main.py

@image html HW1_Image.png

This is an file used for running the elevator tasks

Source code is available at https://bitbucket.org/spinb17/mechatronicslab/src/master/Hw%201/Main.py

@author Ben Spin

@Date 10/6/2020


'''



from HW1_Spin import Button, MotorDriver, TaskElevator

## Motor
Motor_1 = MotorDriver()

## Button for first floor
Button_1_1 = Button('PA6')

## Button for seccond floor
Button_2_1 = Button('PA7')

## Sensor for bottom floor
First_1 = Button('PA8')

## Sensor for seccond floor
Seccond_1 = Button('PA9')



## Task Object
task1 = TaskElevator(.1,'Elevator 1',Motor_1,Button_1_1, Button_2_1, First_1, Seccond_1)

## Motor
Motor_2 = MotorDriver()

## Button for first floor
Button_1_2 = Button('PB6')

## Button for seccond floor
Button_2_2 = Button('PB7')

## Sensor for bottom floor
First_2 = Button('PB8')

## Sensor for seccond floor
Seccond_2 = Button('PB9')

## Task Object
task2 = TaskElevator(.2,'Elevator 2', Motor_2,Button_1_2, Button_2_2, First_2,Seccond_2)

# Continually Run tasks 
for N in range(10000000):
    task1.run()
    task2.run()

