'''
@file LED_main.py

@brief This is the file that will run tasks on our nucleo
@details This should be renamed main.py in order to automatically run on the nucleo for the course of the lab

You can find the source code here : https://bitbucket.org/spinb17/mechatronicslab/src/master/Lab%202/LED_main.py

@author Ben Spin

@date Octover 14,2020

'''
from Lab_02_Spin import TaskFlasher, TaskLEDSINSAW

## Task object 1 for the flashing 
task1 = TaskFlasher(1)
## Task object 2 for the Saw wave
task2 = TaskLEDSINSAW(1,10,30)

while True:
    task1.run()
    task2.run()