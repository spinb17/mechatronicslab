
'''
@file Lab_02_Spin.py


@brief This file houses code to fufill Lab_02 deliverables. It involves an commands sent to a nucleo to vary the brightness of the led

@details This code serves two functions. Firstly it will modulate an imaginary Led between on and off at a set interval.

Seccondly it will control led intensity corresponding to the provided sinusodal function and saw at 30 secconds

Source code is available at  https://bitbucket.org/spinb17/mechatronicslab/src/master/Lab%202/Lab_02_Spin.py 

@author Ben Spin

@date October 14,2020
'''
import utime
import pyb
import math

class TaskFlasher:
    
    '''
    
    @brief A finite state machine that controls an Led On and Off
    
    @details This class implements a finite state machine that is meant to control the light of an LED
    
    '''
    ## State 0 - Sarting State
    S0_INIT = 0;
    
    ## State 1 - Sarting State
    S1_ON = 1;
    
    ## State 2 - Sarting State
    S2_OFF = 2;
    
    def __init__(self, interval):
        
        '''
        @brief Creates a TaskFlasher Object
        @param interval determines how fast the FSM will run

        '''
        
        
        ## Starting state, where every iteration will begin
        self.state = self.S0_INIT
        
        ## Run counter, how many iterations have been run
        self.runs = 0
        
        ## The time stamp for the first itteration
        self.start_time = utime.tick_us()
        
        ## The interval in time in secconds, between tasks
        self.interval = int(interval*1e6) ##Converting the interval to secconds
        
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time , self.interval)
        
    def run(self):
        '''
        @brief Runs one iteration of the task
        '''
        ## Updating Current Time stamp to account for current time
        self.curr_time = utime.ticks_us()  
        if (utime.ticks_diff(self.curr_time,self.next_time)>=0):
            
            if(self.state == self.S0_INIT):
                #State 0 Code
                
                self.transitionTo(self.S1_ON)
                print(str(self.runs)+'ON' )
                
            elif(self.state == self.S1_ON):
                #State 1 Code
                
                self.transitionTo(self.S2_OFF)
                print(str(self.runs)+'OFF')
                
            elif(self.state == self.S2_INIT):
                #State 1 Code
                
                self.transitionTo(self.S1_ON)
                print(str(self.runs)+'ON')
                
            else:
                # Invalid state code (error handling)
                pass
            
            self.runs += 1
            
            # Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.interval,self.next_time) ## Updatating next time
            
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
       
        self.state = newState

            
        
class TaskLEDSINSAW:
    
    '''
    @brief A finite state machine modulating the brightness of an Led
    
    '''
    
    
    ## State 0 - initiation sin
    S0_INIT = 0;
    
    ## State 1 - printing and calcing sin
    S1_CALC_SIN = 1; 
    
     ## State 2 - initiating saw
    S2_INIT2 = 2;
    
    ## State 3 - printing and calcing saw
    S3_CALC_SAW = 3; 
    
   
    def __init__(self,interval,period,switch):
        '''
        @brief Initiates the blinking LED task on our nucleo
        @param interval determines how quickly the FSM will run for
        @param period input for the period between saw waves
        @param switch the time to switch to the saw pattern
        '''
    
        ## Starting state, where every iteration will begin
        self.state = self.S0_INIT
        
        ## Run counter, how many iterations have been run
        self.runs = 0
        
        ## The time stamp for the first itteration
        self.start_time = utime.tick_us()
        
        ## The interval in time in secconds, between tasks
        self.interval = int(interval*1e6) ##Converting the interval to secconds
        
        ## The period between (will be used only if I want to include the saw)
        self.period = int(period*1e6) ##Converting the interval to secconds
        
        ## The time to switch to saw
        self.switch = int(switch*1e6)
        
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time , self.interval)
        
    def run(self):
            '''
            @brief Run one iterattion of the task
            
            '''
        
            self.curr_time = utime.ticks_us()  ## Updating Current Time stamp to account for current time
            if (utime.ticks_diff(self.curr_time,self.next_time)>=0):
            
                    if(self.state == self.S0_INIT):
                        ## Run State 0, Initiating phase and reset
                        self.transitionTo(self.S1_CALC_SIN)
                        pinA5 = pyb.Pin (pyb.Pin.cpu.A5, pyb.Pin.OUT_PP) ## Setting pinA5 
                        pinA5.low() ##Setting LED to OFF
                        
                    elif(self.state== self.S1_CALC_SIN):
                        ## Run State 1 Printing and Calcing Next
                        pinA5 = pyb.Pin (pyb.Pin.cpu.A5)
                        tim2 = pyb.Timer(2, freq=20000)
                        t2ch1= tim2.channel (1,pyb.Timer.PWM, pin=pinA5)
                        ## creating an equation of brightness to match given wave
                        t2ch1.pulse_width_percent(math.sin(int(.15*3.14*self.interval*self.runs/1e6))*100) 
                                                  
                                                  
                        if (int(self.interval*self.runs) >= self.switch):
                              self.transitionTo(self.S2_INIT2)
                              self.runs = -1
                            
                    elif(self.state == self.S2_INIT2):
                        ## Run State 2, Initiating phase and reset
                        self.transitionTo(self.S3_CALC_SAW)
                        pinA5 = pyb.Pin (pyb.Pin.cpu.A5, pyb.Pin.OUT_PP) ## Setting pinA5 
                        pinA5.low() ##Setting LED to OFF
                        
                    elif(self.state == self.S3_CALC_SAW):
                        ## Run State 3 Printing and Calcing Next
                        pinA5 = pyb.Pin (pyb.Pin.cpu.A5)
                        tim2 = pyb.Timer(2, freq=20000)
                        t2ch1= tim2.channel (1,pyb.Timer.PWM, pin=pinA5)
                        ## creating an equation of brightness to match given wave
                        t2ch1.pulse_width_percent((((self.interval*self.runs)/(self.period))*100))
                        
                        
                        if (int(self.interval*self.runs)>=self.period):
                            self.transitionTo(self.S2_INIT2)
                            self.runs = -1
                        
                        
                             
                    else:
                    # Invalid state code (error handling)
                        pass
                    
            self.runs += 1
            
            # Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.interval,self.next_time) ## Updatating next time
            
def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        
        ## Defines the current state of the code that is running
        self.state = newState
 