

''' 
@file Lab_01_Fib.py

@brief This file retuns imput idx and returns a fibonacci number.

@details This .py file was constructed to fulfill requirments for ME 350 lab_01

Acesss this at https://bitbucket.org/spinb17/mechatronicslab/src/master/Lab1/Lab_01_Fib.py

@author Ben Spin

@date October 7, 2020

  '''

def fib (idx):
    '''
    @brief Fib calculates the value value of a fibonnaci sequence at some index
    @param idx the index value that will be input to be solved
 
    '''
        
    print('Calculating Fibonnaci number at index ' + str(idx))
    
     
    if idx < 0:
         print ('Cannot Determine Fibonacci sequence for negative integers... ')
     
    elif idx == 0:
         print ('Your calculated Fibonnaci Number from input 0 is...')
         counter = 0
         print ('0')
    elif idx == 1:
          print ('Your calculated Fibonnaci Number from input 1 is...')
          counter = 1
          print ('1')
    elif idx>1:
         counter = [0,1]
         i = 2
         while i<= idx: #runs until final index is reached
             counter.append(counter[i-2]+counter[i-1]) #Looping value calculating new Fib numbers
             i = i+1 # i is increasing for each itteration
             
         print ('Your calculated Fibonnaci Number from input '+str(idx)+ ' is ' + str(counter[idx]))
         
         print('If youre curious, all previous values are as follows...')
         
         print(counter)
         

        
       
       

if __name__ == '__main__':
    
    ## N is the counter for the range 
    N=0
    for N in range (100000000000000):
        
        ## text is the input from the user at which index to run the Fibonnaci number at
        text = input('Enter an index to find corresponding Fibonnaci Number: ')
        ## value is the integer of text
        value = int(text)
        
        if value < 9999:
            fib(value)
        
        else:
            print ('Values this high will crash the program, please reduce index value')
            
