'''
@file Encoder_Class.py

@brief The Encoder Class sets up methods used in all other Files for this Lab 

The Encoder Class houses several important methods. 

A constructot __init__ that sets appropriate pins and timer use
Update() which is used to regularly record the current position of the encoder
get_position() which returns the recently updated postion of the encoder
set_position() which resets the position to a specified value

You can find this code at : https://bitbucket.org/spinb17/mechatronicslab/src/master/Lab_03/Encoder_Class.py

@package encoder
The conder is a task and a class that interprets and take inputs

@author Ben Spin
@date October 21, 2020
'''
import pyb


class Encoder:
    
           
    '''
    
    @brief Defining modules to be run throught the Encoder Class
    
    @details This class houses important methods used throughout code
    
    '''
    
    def __init__(self, timer, pincom1, pincom2):
        '''
        @brief Does setup for the user, noting pins and timer to use
        @param timer sets the timer for the Encoder class to use
        @param pincom1 dictates the pin used for pin1
        @param pincom2 dictates the pin used for pin2
        '''
        
        ## timer is the timer used for setup
        self.timer = timer
        
        ##set position essentially zeroes values
        self.set_position()
        
        ## determines pin 1
        self.pincom1 = pincom1
        
        ## determines pin 2
        self.pincom2 = pincom2
        
        ##setting up timer channel1
        self.tim.channel(1, pin=self.pincom1 , mode=pyb.Timer.ENC_AB)
        
        ##setting up timer channel2
        self.tim.channel(2, pin=self.pincom2 , mode=pyb.Timer.ENC_AB)

        
    def get_position(self):
        '''
        @brief returns the most recently updated position of the encoder
        '''
        self.next = self.tim.counter()
        
    def get_delta(self):
        '''
        @brief reccords difference in in positions between time of last update
        '''
        self.get_position()
        self.delta = self.next - self.curpos
        
    def update(self):
        '''
        @brief regularly updates the reccorded position of the encoder
        '''
        self.get_delta()
        
        ## Checking for bad delta values
        if (self.delta>0):
            if (self.delta<((0xFFFF)*(1/2))):
                self.curpos = self.curpos + self.delta
            else:
                self.delta =  self.delta - 0xFFFF
                self.curpos = self.curpos + self.delta
        
        ## Checking for bad deltas in the opposite direction
        else:
            if(self.delta>((0xFFFF)*-(1/2))):
                self.curpos = self.curpos + self.delta
            else:
                self.delta = self.delta + 0xFFFF
                self.curpos = self.curpos + self.delta
                

                
    def set_position(self):
        '''
        @brief resests the position to a specified value. This is in our case 0
        '''
        
        ##sets up tim 1
        self.tim = pyb.Timer(self.timer)
        
        ##initializing tim 1
        self.tim.init(prescaler=0, period=0xFFFF)
        
        ##current position to 0
        self.curpos = 0
        
        ##next position to 0
        self.next = 0
        
        ##next delta to 0
        self.delta = 0
        
        ##initializing counter for the encoder
        self.tim.counter()
        
