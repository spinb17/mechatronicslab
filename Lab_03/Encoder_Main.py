'''
@file Encoder_Main.py

@brief The is the main file wich runs both the Encoder and User Task

By running the two files at the same time there is interplay between waiting for excepted inputs 
and the proper manipulation of the encoder.

You can find this code at: https://bitbucket.org/spinb17/mechatronicslab/src/master/Lab_03/Encoder_Main.py


@author Ben Spin
@date October 21, 2020

'''


from Encoder_Class import Encoder
import pyb

## Initializing an encoder
Encoder_OP = Encoder( 3, pyb.Pin.cpu.A6, pyb.Pin.cpu.A7)

from Encoder_Task import TaskEncoder
from User_Task import TaskUI

## Running first encoder task
task0 = TaskEncoder(Encoder_OP, 1, False, 1)

## First task running that is task UI
task1 = TaskUI(1, False , 2)



## The task list contains the tasks to be run "round-robin" style
taskList = [task0,
            task1]

## Running codes on repeat to use as a main.py
while True:
    for task in taskList:
        task.run()