'''
@file Encoder_Task.py

@brief This Task Takes an Input and Runs a corresponding Function



This task takes an input from User_Task and manipulates the encoder accordingly.
Three inputs are excepeted Z which Zeros the encoder position, P which prints the encoder position, and D which prints out
the encoder delta

You can find this code at : https://bitbucket.org/spinb17/mechatronicslab/src/master/Lab_03/Encoder_Task.py

@package Encoder
The encoder is a task and a class that interprets and take inputs

@author Ben Spin
@date October 21, 2020
'''

# Importing Encoder
import utime
import shares

class TaskEncoder:
    
        
    '''
    
    @brief A finite state machine that controls our encoder
    
    @details This class implements an Encoder task that takes Inputs and runs a corresponding task
    
    '''
    ## Defining State Zero, Init
    S0_INIT = 0
    
    ##Defining State One, Waiting For Command
    S1_UPDATE = 1
    
    def __init__(self, Encoder, interval, dbg, taskNum):
        '''
        @brief Creates a TaskEncoder Object
        '''

        ## Referencing which task number we are in
        self.taskNum = taskNum
        
        ##Defining the ecoder
        self.Encoder = Encoder
        
        ## State for running next iteration of the task.
        self.state = self.S0_INIT
        
        ## Time stamp for first Iteration
        self.start_time = utime.ticks_us() # Time stamp in micro secconds
        
        ## Interval of time in secconds between runs of the task
        self.interval = int(interval*1e6)
        
        ## Time stamp for when to run next iteration of the task
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        ## Counter to keep track of how many times our code has run
        self.runs = 0
        
        # Error State Managemnet
        self.dbg = dbg
        
    def run(self):
        '''
        @brief Runs one iteration of the task
        '''
         
        ##updating time stamp for next run
        self.curr_time = utime.ticks_us() ## Updating time stamp for each run
        if (utime.ticks_diff(self.curr_time, self.next_time) >= 0):
            if (self.state == self.S0_INIT):
                ## Run portion of state 0 code
                self.transitionTo(self.S1_UPDATE)
                self.printTrace()
            
            elif (self.state == self.S1_UPDATE):
                self.Encoder.update()
                self.transitionTo(self.S1_UPDATE)
                
                if(shares.cmd):
                    self.UI()
                    shares.cmd = None
                    
            self.printTrace()
            
        else:
            #Error Handling
            pass
        
        self.runs += 1
        self.next_time = utime.ticks_add(self.next_time , self.interval) #Adjusting to the new next time
            
    def transitionTo(self,  newState):
         '''
        @brief Updating State Variable
         
         '''
         self.state = newState
       
    def printTrace(self):
         '''
        @brief debug statement from charlies lecture that can be turned off by altering dbg to False
         '''
         
         if self.dbg:
             str = 'T{:}/S{:}/R{:}\t@ {:9.0f}:'.format(self.taskNum, self.state, self.runs, utime.ticks_diff(self.curr_time,self.start_time))
             print(str)
                
    def UI(self):
        '''
        @brief recognizing user commands and determining if shares falls within one of several catagories
        '''
        if shares.cmd in [90, 122]: ##ASCII Charecter Z, or z
            self.Encoder.set_position()
            print(str('Charecter Z Recognized. Position has been reset to Zero'))
            
        elif shares.cmd in [80, 112]: ##ASCII Charecter P, or p
            
            resp = self.Encoder.curpos
            print(str('Charecter P Recognized. Your Current Position is ') + str(resp))
            
        elif shares.cmd in [68, 100]: ##ASCII Charecter D, or d
            
            resp = self.Encoder.delta
            print(str('Your last calculated delta was ') + str(resp))
            
        else:
            
            resp = 'Charecter not Recognized. Please enter Z P or D'
            print(str(resp))
        pass
