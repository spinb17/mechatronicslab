'''
@file User_Task.py

@brief This Task Waits for input from the User 
@details The idea with this task is to wait for an Input from the User and constantly update in 
order to not miss any inputs. It reacts directly with the Encoder Task

You can find this code at : https://bitbucket.org/spinb17/mechatronicslab/src/master/Lab_03/User_Task.py


@author Ben Spin
@date October 21, 2020
'''

import utime
import shares
from pyb import UART

class TaskUI:
    
           
    '''
    
    @brief A finite state machine that waits for an input from the User
    
    @details This class implements an UI task that continously loops and waits for the user to enter in an eccepted command
    
    '''
    ## Defining State Zero, Init
    S0_INIT = 0
    
    ##Defining State One, Waiting For Command
    S1_WAITING_FOR_CHAR = 1
    

    
    def __init__(self, interval, dbg, taskNum):
        '''
        @brief Creates a TaskUser Object
        @param interval determines the speed at which the FSM will run
        @param dbg determines whether debug is on, resulting in a trace beigning printed for the user
        @param taskNum designates the number of the task as it is running
        '''
        ## The number of the task
        self.taskNum = taskNum
        
        ##  The amount of time in microseconds between runs of the task
        self.interval = int(interval)
        
        ## Flag to print debug messages or supress them
        self.dbg = dbg
    
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us()
        
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)

        ## Serial port
        self.ser = UART(2)
        
    def run(self):
        '''
        @brief Runs one iteration of the task
        '''
        ## The timestamp for the current iteration (or most recent iteration) of the task. This is with respect to an arbitrary initial time and must be used differentially.
        self.curr_time = utime.ticks_us()
        
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
            
            if(self.state == self.S0_INIT):
                print('Enter Z to zero the Encoder/Enter P to Print Encoder Position/Enter d to Print Encoder Delta')
                # Run State 0 Code
                self.transitionTo(self.S1_WAITING_FOR_CHAR)
            
            elif(self.state == self.S1_WAITING_FOR_CHAR):
                # Run State 1 Code
                if self.ser.any():
                    self.transitionTo(self.S1_WAITING_FOR_CHAR)
                    shares.cmd = self.ser.readchar()

            else:
                # Invalid state code (error handling)
                pass
            
            self.runs += 1
            
            # Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.next_time, self.interval)
            
    def transitionTo(self, newState):
        '''
        @brief Updates the state variable
        '''
        self.state = newState

    def printTrace(self):
        '''
        @brief Prints a debug statement with a detailed trace message if the debug variable is set
        '''
        if self.dbg:
            str = 'T{:}/S{:}/R{:}\t@ {:9.0f}:'.format(self.taskNum, self.state, self.runs, utime.ticks_diff(self.curr_time,self.start_time))
            print(str)
            
