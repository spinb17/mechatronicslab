'''
@file shares.py
@brief A container for all the inter-task variables

@package shares
Shares represents the two values that will be shared within files

see file at : https://bitbucket.org/spinb17/mechatronicslab/src/master/Lab_03/shares.py

@author Ben Spin
@date October 21, 2020
'''

## The command character sent from the user interface task to the cipher task
cmd     = None

## The response from the cipher task after encoding
resp    = None