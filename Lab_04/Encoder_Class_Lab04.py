'''
@file Encoder_Class_Lab04.py

@brief The Encoder Class sets up methods used in all other Files for this Lab 

The Encoder Class houses several important methods. 

This is the updated Encoder for use in Lab 4 some features have been removed or modified to better fit our need for Lab04

A constructot __init__ that sets appropriate pins and timer use
Update() which is used to regularly record the current position of the encoder
get_position() which returns the recently updated postion of the encoder
set_position() which resets the position to a specified value

You can find this code at : https://bitbucket.org/spinb17/mechatronicslab/src/master/Lab_04/Encoder_Class_Lab04.py

@package encoder
The conder is a task and a class that interprets and take inputs

@author Ben Spin
@date November 4, 2020
'''
import pyb


class Encoder:
    
           
    '''
    
    @brief Defining modules to be run throught the Encoder Class
    
    @details The econder class houses code related to reading and controlling the encoder that is attached to our motors
    
    '''
    
    def __init__(self, timer, pincom1, pincom2):
        '''
        @brief Does setup for the user, noting pins and timer to use
        @param timer dictates which timer will be used for this encoder setup
        @param pincom1 dictates the first pin used for the encoder channel
        @param pincom2 dictates the secceond pin to be used for the seccond encoder channel
        '''
        
        ## Timer sets which timer this encoder will use
        self.timer = timer
        
        ## Setting initial positions for the encoder
        self.set_position()
        
        ## Setting pin communication one, or the pin number
        self.pincom1 = pincom1
        ## Setting pin communication two, or the pin number 
        self.pincom2 = pincom2
        
        ## Setting variables for channel 1
        self.tim.channel(1, pin=self.pincom1 , mode=pyb.Timer.ENC_AB)
        ## Setting variables for channel 2
        self.tim.channel(2, pin=self.pincom2 , mode=pyb.Timer.ENC_AB)

        
    def get_position(self):
        '''
        @brief returns the most recently updated position of the encoder
        '''
        self.next = self.tim.counter()
        
    def get_delta(self):
        '''
        @brief reccords difference in in positions between time of last update
        '''
        self.get_position()
        self.delta = self.next - self.curpos
        
    def update(self):
        '''
        @brief regularly updates the reccorded position of the encoder
        @details This class is also responsible for making sure that we get no "bad deltas" as discussed in lecture
        '''
        
        ## checking for bad deltas
        self.get_delta()
        if (self.delta>0):
            if (self.delta<((0xFFFF)*(1/2))):
                self.curpos = self.curpos + self.delta
            else:
                self.delta =  self.delta - 0x10000
                self.curpos = self.curpos + self.delta
        
        ## checking for bad deltas in the other direction
        else:
            if(self.delta>((0xFFFF)*-(1/2))):
                self.curpos = self.curpos + self.delta
            else:
                self.delta = self.delta + 0x10000
                self.curpos = self.curpos + self.delta
                

                
    def set_position(self):
        '''
        @brief resests the position to a specified value. This is in our case 0
        '''
        
        ##sets up tim 1
        self.tim = pyb.Timer(self.timer)
        
        ##initializing tim 1
        self.tim.init(prescaler=0, period=0xFFFF)
        
        ##current position to 0
        self.curpos = 0
        
        ##next position to 0
        self.next = 0
        
        ##next delta to 0
        self.delta = 0
        
        ##initializing counter for the encoder
        self.tim.counter()