'''
@file L4_main.py

@brief The the file running in the background for Lab 4 that is responsible for setting up tasks
@details This runs our Lab4_DataCollector on the nucleo on loop and should be renamed main.py for the purposes of it automatically running 
in the background while the UI waits for an input from the user

You can find this code at: https://bitbucket.org/spinb17/mechatronicslab/src/master/Lab_04/L4_main.py


@author Ben Spin
@date November 4, 2020

'''


from Encoder_Class_Lab04 import Encoder
## from Encoder_Task_Lab04 import TaskEncoder
from Lab4_DataCollector import dataCollector
import pyb

## Initializing first encoder
Encoder_OP = Encoder( 3, pyb.Pin.cpu.A6, pyb.Pin.cpu.A7)

## Setting up the task 1 data collector
task1 = dataCollector(Encoder_OP, .2 )

## Looping task to run automatically
while True:
    task1.run()