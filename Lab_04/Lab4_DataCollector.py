'''
@file Lab4_DataCollector.py

@brief Data Collector takes inputs from the UI front end and reacts accordingly 



dataCollector takes input G or S and either starts or stops data collection

You can find this code at : https://bitbucket.org/spinb17/mechatronicslab/src/master/Lab_04/Lab4_DataCollector.py



@author Ben Spin
@date November 4, 2020
'''
import utime
from pyb import UART




class dataCollector:
   '''
   @brief Creates a class dataCollector
   
   @details This task is ran on the nuceo in order to interpret commands from the user and collect data from
   the encoder in order to generate plots within the UI section of this code
   '''
    
    
   ## State 0 Initializer
   S0_INIT = 0
   
   ## State 1 Waiting for user imput
   S1_WAIT_FOR_USER = 1
   
   ## State 2 Collecting data
   S2_COLLECT_DATA = 2
    
   def __init__(self, Encoder, interval):
        '''
        @brief Creates a dataCollector Object
        @param Encoder determines which encoder we will be using to collect data
        @param interval determines the rate at which our FSM runs
        '''
    
       

        ## Setting a self for the encoder that we will use in this lab
        self.Encoder = Encoder
        
        ## State for running next iteration of the task.
        self.state = self.S0_INIT
        
        ## Time stamp for first Iteration
        self.start_time = utime.ticks_us() # Time stamp in micro secconds
        
        ## Interval of time in secconds between runs of the task
        self.interval = int(interval*1e6)
        
        ## Time stamp for when to run next iteration of the task
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        ## Counter to keep track of how many times our code has run
        self.End_Time = 0
        
        ## Our UART communication
        self.myuart = UART(2)
        
        ## Time State
        self.time = []
        
        #Motor Position
        self.motor_pos = []
    
   def run(self):
       
        '''
        @brief Runs One Iteration of the Task
        '''
        
        ## Updating Time Stamp
        self.curr_time = utime.ticks_us() ## Updating time stamp for each run
        if (utime.ticks_diff(self.curr_time, self.next_time) >= 0):
            if (self.state == self.S0_INIT):
                ## Run portion of state 0 code
                self.transitionTo(self.S1_WAIT_FOR_USER)
               
                
            elif (self.state== self.S1_WAIT_FOR_USER):
                
                if self.myuart.any() != 0:
                    
                    var = self.myuart.readchar()
                    if var in [103, 71]: #ASCII g or G
                    
                        self.transitionTo(self.S2_COLLECT_DATA)
                        self.End_Time = utime.ticks_us()
                        
                        
                    else:
                        
                        self.transitionTo(self.S1_WAIT_FOR_USER)
                        

                    
            elif (self.state == self.S2_COLLECT_DATA):
                self.Encoder.update()
                
                if self.myuart.any() != 0:
                    var = self.myuart.readchar()
                 
                    if var in [83, 115]:
                        self.transitionTo(self.S1_WAIT_FOR_USER)
                        self.myuart.write('End')
                        
                elif (utime.ticks_diff(utime.ticks_us() , self.End_Time) > (10*1e6)):
                    self.transitionTo(self.S1_WAIT_FOR_USER)
                    self.myuart.write('End')
                        
                else:
                    utime.sleep(.2)
                    self.transitionTo(self.S2_COLLECT_DATA)
                    self.time = utime.ticks_diff(utime.ticks_us(), self.End_Time)
                    self.motor_pos = self.Encoder.curpos
                    self.myuart.write('{:},{:}\r\n'.format(self.time,self.motor_pos))
            else:
            #Error Handling
                pass
        
        
        ## Adding interval for next run time
        self.next_time = utime.ticks_add(self.next_time , self.interval) #Adjusting to the new next time
            
   def transitionTo(self,  newState):
         '''
        @brief Updating State Variable
         
         '''
         self.state = newState
       

                
                    