'''
@file UI_Lab04.py

@image html FSM_UI_Lab04.png

@image html L4_Task_Diagram.png

@brief The is the front end for our lab 4 that runs the user interface functions

@details This takes an input g, and then a timeout or charecter s to stop. It also graphs the values and reurns an excel file populated with values for time and motor position

You can find this code at: https://bitbucket.org/spinb17/mechatronicslab/src/master/Lab_04/UI_Lab04.py


@author Ben Spin
@date November 4, 2020

'''

import serial
import matplotlib.pyplot as plt
import time
import numpy as np

## Setting up serial port
ser = serial.Serial (port='Com3', baudrate=115200 , timeout=10)
print ('Enter G to Begin Data Collection, and when you are satisfied Enter S to end collection. ')
print ('Simulation, unless stopped with the S commond will run for 10 secconds')
## Hold value marking if a charecter has been entered
char_check = False 

while char_check == False:
    ## The entered command from the user
    data_start = input('Enter Command: ')
    if(data_start=='G' or data_start=='g'):
        ## Housing eventual return to me code
        char_check = True
        ## Writing Data Start
        ser.write(str(data_start).encode('ascii'))
        ## counter
        n=1
    else:
        print ('Enter G to begin data collection')
        

## Grabs timing of where code is currently standing         
init_t = time.time()         
        
while (char_check == True and time.time()<init_t + 10):
    ## Variable for the entered value from user
    data_stop = input('Data is being collected. This program will run automically for 10 secconds. Press s to terminate: ')
    if (data_stop=='s' or data_stop=='S'):
        char_check = False
        #Stop data collection
        ser.write(str(data_stop).encode('ascii'))
    elif (time.time()>= init_t+10):
        print('10 Seccond elapsed. The program has been terminated')
        
    else :
        print('Invalid Command')
        


## Time List   
t = []
## Motor Position List
motor_pos=[]

## First Value is 0
value = 0
## Stripped Values are 0
values = 0
## Run number is 0
run = 0



while True:
    ## Direct Read
    value = ser.readline()
    ## Stripping value of special charecters
    change = value.decode()
    ## Splitting change at apostrophy point
    values = change.strip().split(',')

    if values != ['End']:
        t.append(int(values[0])*(1e-6))
        motor_pos.append(int(values[1])*(360/4096))
    else:
        break

   

ser.close()

    
plt.plot(t,motor_pos)
plt.ylabel('Motor Value In degrees')
plt.xlabel('Time [s]')
plt.title('Motor Response')

np.savetxt('DegvTime.csv',[t,motor_pos],delimiter=',')
    