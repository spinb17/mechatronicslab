'''
@file BLE_Driver_Class.py

@brief This class sets up some methods that our UI can call

@details Can read from the UART as well as turn on and off our LED light

You can find this code at : https://bitbucket.org/spinb17/mechatronicslab/src/master/Lab_05/BLE_Driver_Class.py

@package encoder
The conder is a task and a class that interprets and take inputs

@author Ben Spin
@date November 12, 2020
'''
import pyb
from pyb import UART

class BLE_Driver:
    
           
    '''
    
    @brief Defining modules to be run throught the BLE_Driver
    
    @details This class houses the mthods for controlling the LED 
    
    '''
    
    def __init__(self,uart_num,baud_rate):
        '''
        @brief Initializes a BLE driver 
        @param uart_num Imputs a uart number to be used for communication with the nucleo
        @param buad_rate Sets the buad_rate for communication
        '''

        
        ## Setting up the uart
        self.myuart = UART(uart_num,baud_rate)
        
        ## Initializing the UART
        self.myuart.init(baud_rate)
        
        ## Pin A5 which is used to flash the LED
        self.pinA5 = pyb.Pin (pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)


        
    def read(self):
        '''
        @brief reads the value within the REPL
        '''
        myval = self.myuart.read()
        return myval
        
    def write(self,val):
        '''
        @brief Writes a value to the REPL
        '''
        self.myuart.write(val)
        
    def check(self):
        '''
        @brief Checks if any values are waiting within the REPL
        '''
        check = self.myuart.any()
        return check
    
    def TurnOn(self):
        '''
        @brief Turns the LED ON
        '''
        self.pinA5.high()
        
    def TurnOff(self):
        '''
        @brief Turns the LED OFF
        '''
        self.pinA5.low()
                

