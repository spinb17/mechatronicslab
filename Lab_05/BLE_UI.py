'''
@file BLE_UI.py

@image html FSM_UI_Lab05.png


@brief The is the front end for our lab 5 that runs the user interface functions

Takes an input 1-10 and blinks the LED at the corresponding frequency 


You can find this code at: https://bitbucket.org/spinb17/mechatronicslab/src/master/Lab_05/BLE_UI.py


@author Ben Spin
@date November 4, 2020

'''

import utime
from pyb import UART
from BLE_Driver_Task import BLE_Driver



class BLEUI:
   '''
   @brief Creates a class dataCollector
   
   @details This task is ran on the nuceo in order to interpret commands from the user
   '''
    
    
   ## State 0 to initialize
   S0_INIT = 0
   
   ## State 1 which stands and waits for user imput
   S1_WAIT_FOR_USER = 1
   
   ## State 2 which is the state where the LED flasher is on 
   S2_LED_FLASH_ON = 2
   
   ## State 3 which is the state where the LED flasher is off
   S3_LED_FLASH_OFF = 3

    
   def __init__(self, interval):
        '''
        @brief Creates a dataCollector Object
        @param interval Sets the interval that the FSM will run at
        '''
    
        ## Sets up the BLE driver so that we can manipulate our LED and read from serial       
        self.BLE = BLE_Driver(3,9600)
        
        ## State for running next iteration of the task.
        self.state = self.S0_INIT
        
        ## Time stamp for first Iteration
        self.start_time = utime.ticks_us() # Time stamp in micro secconds
        
        ## Interval of time in secconds between runs of the task
        self.interval = int(interval*1e6)
        
        
        ## Time stamp for when to run next iteration of the task
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        ## Setting up the UART
        self.myuart = UART(3)
        
        ## Wait value for when flasher is running
        self.wait = 0
        

    
   def run(self):
       
        '''
        @brief Runs One Iteration of the Task
        '''
        
        ## Updating time stamp for ueach run
        self.curr_time = utime.ticks_us() 
        if (utime.ticks_diff(self.curr_time, self.next_time) >= 0):
            if (self.state == self.S0_INIT):
                ## Run portion of state 0 code
                self.transitionTo(self.S1_WAIT_FOR_USER)
               
                
            elif (self.state== self.S1_WAIT_FOR_USER):
                
                
                
                if self.BLE.check() != 0 :
                    
                    ## Reading from the phone the imput frequency
                    self.tmpFreq = self.BLE.read()
                    
                    ## Turning temp frq into a ussable integer
                    self.FRQ = int(self.tmpFreq)
                    
                    if self.FRQ > 0 and self.FRQ <= 10 :
                        self.interval = int(5e5/(self.FRQ))
                        self.transitionTo(self.S2_LED_FLASH_ON)
                    
                    else:
                        
                        print('Only Enter Frequencies Between 1 and 10')
                
                else:
                    
                    self.transitionTo(self.S1_WAIT_FOR_USER)
                        
            ## Flasher portion of the LED. This is modeled as one state. It is broken in two to take advantage of the interval
                    
            elif (self.state == self.S2_LED_FLASH_ON):
                
                
                self.BLE.TurnOn()
                
         
                if self.BLE.check() != 0:
                    
                    self.transitionTo(self.S1_WAIT_FOR_USER)
                    
                else:
                    self.transitionTo(self.S3_LED_FLASH_OFF)
                    
            
            elif (self.state == self.S3_LED_FLASH_OFF):
                
                self.BLE.TurnOff()
                
                if self.BLE.check() != 0:
                    
                    self.transitionTo(self.S1_WAIT_FOR_USER)
                    
                else:
                    self.transitionTo(self.S2_LED_FLASH_ON)
                
            ## Adding interval in order to adjust to new time    
            self.next_time = utime.ticks_add(self.next_time , self.interval) #Adjusting to the new next time
            
   def transitionTo(self,  newState):
         '''
        @brief Updating State Variable
         
         '''
         self.state = newState
       

                
                    