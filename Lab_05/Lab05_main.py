'''
@file Lab05_main.py

@brief This is the file that will run tasks on our nucleo
@details This is the code that should be named and run as main.py on our nucleo. This enables
the code to run automatically in the background and be ready to take in inputs. It is also responsible for setting up the tasks
in this lab.

You can find the source code here : https://bitbucket.org/spinb17/mechatronicslab/src/master/Lab_05/main.py

You can find a link to the thunkable portion of this lab here: https://x.thunkable.com/copy/c6c66377fafbbb701dba63b619aad86e

@author Ben Spin

@date November 12,2020

'''

              
from BLE_UI import BLEUI


# Setting frequency for BLEUI task 1
task0 = BLEUI(.2)





## The task list contains the tasks to be run "round-robin" style
taskList = [task0]

## Running tasks constantly as a main.py file
while True:
    for task in taskList:
        task.run()