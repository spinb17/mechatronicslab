'''
@file Lab_06_ClosedLoop.py

@brief Closed Loop holds the calculation for closed loop values 

@details This code holds the tools for our controller in Lab 6

You can find this code at : https://bitbucket.org/spinb17/mechatronicslab/src/master/Lab_06/Lab_06_ClosedLoop.py



@author Ben Spin
@date November 26, 2020
'''



class ClosedLoop:
    
           
    '''
    
    @brief Defining modules to be run throught the Closed Loop Clas
    
    @details This class houses methods used to set and get our closed Loop gain which will be used to control the motor
    as well as an update method which will be used to return the acutation value based on the measured and reference values
    '''
    
    def __init__(self, Kp , Omega_Ref):
        '''
        @brief Does setup for the user, noting pins and timer to use
        @param Kp is the reference initial Kp value passed to ClosedLoop. This is delivered by the user
        @param Omega_Ref is used to set the target speed in RPMS for the controller to aim for
        '''
        ## Setting the initial Kp value for the initialized ClosedLoop
        self.Kp = Kp
        
        ## Setting the reference omega value that we will attempt to drive our motor at 
        self.Omega_Ref = Omega_Ref

    def update(self,speed):
        '''
        @brief Takes an input speed and updates the value of L. When called consistently acts as our controller
        @param speed the current speed that the motor is running at, used as reference for the controller
        '''

        
        L = (self.Kp) * (self.Omega_Ref-int(speed))
        return L
    
    def get_Kp(self):
        '''
        @brief Returns a value Kp
        '''
        return self.Kp
    
    def set_Kp(self,Kp):
        '''
        @brief Sets the value of Kp to a new value
        @param Kp The new value of Kp to be initialized

        '''
        self.Kp = Kp
    
    
        