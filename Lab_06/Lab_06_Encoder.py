'''
@file Lab_06_Encoder.py

@brief The Encoder Class sets up methods used in all other Files for this Lab 

The Encoder Class houses several important methods. 


A constructot __init__ that sets appropriate pins and timer use
Update() which is used to regularly record the current position of the encoder
get_position() which returns the recently updated postion of the encoder
set_position() which resets the position to a specified value

You can find this code at : https://bitbucket.org/spinb17/mechatronicslab/src/master/Lab_06/Lab_06_Encoder.py

@package Encoder
The conder is a task and a class that interprets and take inputs

@author Ben Spin
@date November 26, 2020
'''
import pyb


class Encoder:
    
           
    '''
    
    @brief Defining modules to be run throught the Encoder Class
    
    @details This class houses important methods used throughout code relating to the encoder. The econder records the position of the motor
    and will also be used to determine speeds
    
    '''
    
    def __init__(self, timer, pincom1, pincom2):
        '''
        @brief Does setup for the user, noting pins and timer to use
        @param timer dictates which timer will be used for this encoder setup
        @param pincom1 dictates the first pin used for the encoder channel
        @param pincom2 dictates the secceond pin to be used for the seccond encoder channel
        '''
        
        ## Setting timer 
        self.Timer = timer
        ## Setting up the timer
        self.tim = pyb.Timer(self.Timer , prescaler=0 , period=0xFFFF)
        ## Defining the first pin for the encoder
        self.pincom1 = pincom1
        ## Defining the seccond pin for the encoder
        self.pincom2 = pincom2
        ## Setting up the channel 1 for our Encoder
        self.tim.channel(1, pin=self.pincom1 , mode=pyb.Timer.ENC_AB)
        ## Seting up the channel 2 for our Encoder
        self.tim.channel(2, pin=self.pincom2 , mode=pyb.Timer.ENC_AB)
        ## Calling set position to 0 out the encoder
        self.set_position()

        
    def get_position(self):
        '''
        @brief Returns the most recently updated position of the encoder
        '''
        
        self.update()
        pos_val = self.curpos
        return pos_val
        
    def get_delta(self):
        '''
        @brief Reccords difference in in positions between time of last update
        '''
        self.update()
        del_val = self.delta
        return del_val
        
        
    def update(self):
        '''
        @brief Regularly updates the reccorded position of the encoder
        @details This class is also responsible for making sure that we get no "bad deltas" as discussed in lecture
        '''
        self.next = self.tim.counter()
        self.delta = self.next - self.curpos
        
        if (self.delta>0):
            if (self.delta<((0xFFFF)*(1/2))):
                self.curpos = self.curpos + self.delta
            else:
                self.delta =  self.delta - 0x10000
                self.curpos = self.curpos + self.delta
                
        else:
            if(self.delta>((0xFFFF)*-(1/2))):
                self.curpos = self.curpos + self.delta
            else:
                self.delta = self.delta + 0x10000
                self.curpos = self.curpos + self.delta
                

    def set_position(self):
        '''
        @brief resests the position to a specified value. This is in our case 0
        '''
        ## Defining current position
        self.curpos = 0
        ## Defining next position
        self.next = 0
        ## Defining the change from last position
        self.delta = 0
        ## Setting the counter
        self.tim.counter()