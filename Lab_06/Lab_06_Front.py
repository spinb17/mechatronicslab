
'''
@file Lab_06_Front.py



@brief The is the front end for our lab 6 that runs the user interface functions

This takes an input Kp and returns a graph

It graphs the values and reurns an excel file populated with values for time and motor position

The three images below are the responses of the plot with increasing Kp , and therefore higher performance in the result
Figure 1 has a Kp of .015 , Figure 2 has a Kp of .02 , Figure 3 has a Kp of .025 That seems to work for us. Reference vaule for all is 1440

@image html Kp_lowest_point1.png
@image html Kp_mid_point3.png
@image html Kp_highest_point8.png

You can find this code at: https://bitbucket.org/spinb17/mechatronicslab/src/master/Lab_06/Lab_06_Front.py


@author Ben Spin
@date November 26, 2020

'''
import time
import serial
import matplotlib.pyplot as plt
import numpy as np


## Setting up the serial for serial communication
ser = serial.Serial (port='Com5', baudrate=115200 , timeout=10)
print ('Enter a value of Kp to begin data colletion')
print ('The simulation will run automatically for 2 secconds in order to generate the response graph')

while True:
    try:
        ## start data is the input Kp from our user
        start_data = (input('Enter a Kp Value: '))
        break
    except ValueError:
        print('Please enter an integer value')
        
ser.write(str(start_data).encode('ascii'))

## List of all time variables
t = [0]
## List of all recorded motor speeds
motor_speed=[0]
## Initial value that is the direct read off of the UART
value = 0
## Value after it has been stripped down and interpretted
values = 0
## Counter for the run that the code is currently on
run = 0
time.sleep(5)

while True:
    value = ser.readline()
    ## Decoded values
    change = value.decode()
    values = change.strip().split(',')

    if values != ['End']:
        t.append(float(values[1])*(1e-6))
        motor_speed.append(float(values[0]))
        
    else:
        break



ser.close()
    
plt.plot(t,motor_speed)
plt.ylabel('Motor speed  [rotations/minute]')
plt.xlabel('Time [s]')
plt.title('Motor Response')

np.savetxt('SpeedvTime.csv',[t,motor_speed],delimiter=',')
    