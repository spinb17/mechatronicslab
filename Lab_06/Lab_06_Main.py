'''
@file Lab_06_Main.py

@brief This file will run when a Kp value is entered and inits the other pieces of code. 
@details It is important that this be named main.py within the nucleo so that it will run automatically. This code will be running automatically 
but action will only occur when a Kp has been entered.

Code can be located at : https://bitbucket.org/spinb17/mechatronicslab/src/master/Lab_06/Lab_06_Main.py

@image html Lab_06_TaskDiagram.png

@author Ben Spin
@date November 26 , 2020

'''
import pyb
from Lab_06_ControllerTask import ControllerTask
from Lab_06_Encoder import Encoder
from Lab_06_ClosedLoop import ClosedLoop
from Lab_06_WritingTask import TaskWriting
from Lab_06_Motor import MotorDriver


    
## Setting up the encoder for the Lab
Encoder = Encoder(8, pyb.Pin.cpu.C6 , pyb.Pin.cpu.C7)

## Setting up the motor that is working so that we can collect data
Motor = MotorDriver( pyb.Pin.cpu.A15  , pyb.Pin.cpu.B4 , pyb.Pin.cpu.B5 , 3 , 4 , 3 , 35  )

## Initializing the Kp gain as 0
Kp = 0

## Defining the parameters for closed loop 
Closed_Loop = ClosedLoop(Kp , 1000)

## Defining parameters for the controller task
task1 = ControllerTask( Motor , Encoder , Closed_Loop , 0.02)
## Defining the parameters for the UART writing task
task2 = TaskWriting (0.02)

while True:
    task1.run()
    task2.run()