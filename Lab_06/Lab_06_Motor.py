'''
@file Lab_06_Motor.py

@brief This code holds lab06 Motor Driver. Allowing the user to input duty cycles and spin the motor accordingly

The tasks that this motor driver can do are enable, disable, and set duty

You can find this code in my bit.bucket under : https://bitbucket.org/spinb17/mechatronicslab/src/master/Lab_06/Lab_06_Motor.py

@author Ben Spin
@date November 26 , 2020
'''
import pyb

class MotorDriver:
    ''' 
    @brief This class implements a motor driver for the Balancing Platform Board
    '''
    def __init__ (self, nSLEEP_pin  , IN1_pin , IN2_pin , Ch1 , Ch2, timer ,  min_dut):
        '''
        @brief Creates a motor driver by initializing GPIO pins and turning the 
        motor off for safety
        
        @param nSLEEP_pin A pyb.Pin object to use as the enable pin
        @param IN1_pin    A pyb.Pin object to use as the input to half bridge 1.
        @param IN2_pin    A pyb.Pin object to use as the input to falg bridge 2.
        @param Ch1        A Number representing channel one of the motor object
        @param Ch2        A Number representing channel two of the motor object
        @param timer      A pyb.Timer object to use for the PWM generation on IN1_pin and IN2_pin.
        @param motor_num  An object used to define which motor the class object is meant to interact with
        @param min_dut    A minimum number as determined by the minimum speed at which the motor is able to function
        '''
        
        ## Determines the timer number
        self.Timer = timer
        ## Sleep pin sets up the pin that is used to toggle enableing the motor
        self.nSLEEP_pin = pyb.Pin (nSLEEP_pin, pyb.Pin.OUT_PP)
        ## Determines the first pin for the motor object
        self.IN1_pin = pyb.Pin (IN1_pin)
        ## Determines the seccond pin for the motor object
        self.IN2_pin = pyb. Pin (IN2_pin)
        ## Determines the number for motor channel 1
        self.Ch1 = Ch1
        ## Determines the number for motor channel 2
        self.Ch2 = Ch2
        ## Defines the minimum duty at which the motor is to operate at
        self.min_dut = min_dut
        ## Sets up the timer for the motor
        self.tim = pyb.Timer(self.Timer, freq= 20000)
        ## All variables to be input for the first channel
        self.t3ch1 = self.tim.channel(Ch1, pyb.Timer.PWM , pin= self.IN1_pin )
        ## All variables to be input for the seccond cahnnel
        self.t3ch2 = self.tim.channel(Ch2, pyb.Timer.PWM , pin= self.IN2_pin )
        
        
        
        self.nSLEEP_pin.low()

        
    def enable (self):
        '''
        @brief Enables the motor by setting the sleep pin to high

        '''
        self.nSLEEP_pin.high()
        
    def disable(self):
        '''
        @brief Disables the motor by setting the sleep pin to low

        '''
        self.nSLEEP_pin.low()
        
    def set_duty(self, duty):
        '''
        @brief Sets the duty cycle of the motor with positive values indicating forward direction and negative values indicating backwards direction
        @param duty a signed integer holding the duty cucle of the PWM signal sent to the motor
        '''

        
        self.t3ch1.pulse_width_percent(0)
        self.t3ch2.pulse_width_percent(0)
        
        if duty >= 100:
            self.t3ch1.pulse_width_percent(100)
            self.t3ch2.pulse_width_percent(0)
            
        elif duty <= -100:
            self.t3ch2.pulse_width_percent(100)
            self.t3ch1.pulse_width_percent(0)
            
        elif duty < 0 and duty <= -35:
            duty = abs(duty)
            self.t3ch1.pulse_width_percent(0)
            self.t3ch2.pulse_width_percent(duty)
            
        elif duty > 0 and duty >= 35:
            self.t3ch1.pulse_width_percent(duty)
            self.t3ch2.pulse_width_percent(0)
            
        elif duty < 0 and duty > -int(self.min_dut):
            self.t3ch1.pulse_width_percent(0)
            self.t3ch2.pulse_width_percent(int(self.min_dut))
            
        elif duty > 0 and duty < int(self.min_dut):
            self.t3ch1.pulse_width_percent(int(self.min_dut))
            self.t3ch2.pulse_width_percent(0)
            
        elif duty == 0:
            self.t3ch1.pulse_width_percent(0)
            self.t3ch2.pulse_width_percent(0)
        else:
            
            pass
        

    

    
        