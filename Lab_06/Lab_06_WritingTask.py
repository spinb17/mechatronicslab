'''
@file Lab_06_WritingTask.py

@brief This is the task that is in charge of writing to the UART and recording all the values that we collect

Code can be located at : https://bitbucket.org/spinb17/mechatronicslab/src/master/Lab_06/Lab_06_WritingTask.py

@image html Lab_06_FSM_WritingTask.png

@author Ben Spin
@date November 26 , 2020
'''

import utime
from pyb import USB_VCP
import shares

class TaskWriting:
    '''
    @brief TaskWriting is in charge of recording values and writing them the the UART
    '''
    ## State 0 Initializing
    S0_INIT                 = 0
    
    ## State 1 Waiting for Commands
    S1_WAITING              = 1
    
    ## State 2 Writing to the serial 
    S2_WRITING              = 2
    
    def __init__(self, Interval):
        '''
        @brief Creates a data collecting task object.
        @param Interval An integer number of seconds between desired runs of the task
        '''
        ## Setting up UART for the Nucleo
        self.myuart = USB_VCP()
        
        ## Interval
        self.interval = int(Interval*1e6)
        
        ## Starting Time Stamp
        self.start_time = utime.ticks_us()
        
        ## Next Time for Task to run
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        ## State for running next iteration of the task.
        self.state = self.S0_INIT
        
        ## Stating counter state of 0
        self.counter = 0
        
        
    def run(self):
        '''
        @brief Runs one iteration of the task
        '''
        ## updating the current timestamp
        self.curr_time = utime.ticks_us()    
        
        # checking if the timestamp has exceeded our "scheduled" timestamp
        if (utime.ticks_diff(self.curr_time, self.next_time) >= 0):
            if (self.state == self.S0_INIT):
                # Run State 0 Code
                self.transitionTo(self.S1_WAITING)
                
            elif (self.state == self.S1_WAITING):
                
                if self.myuart.any() == True:
                    shares.Kp = float(self.myuart.readline())
                    self.transitionTo(self.S2_WRITING)
                    self.counter = 0
                
            elif (self.state == self.S2_WRITING):
                if shares.Kp == None:
                    length = len(shares.speed)
                    self.counter += 1

                    
                    if self.counter < length+1:
                        n = int(self.counter)
                        self.myuart.write('{:},{:}\r\n'.format(shares.speed[n-1],shares.time[n-1]))
                        
                    

                    else :
                        
                        self.myuart.write('End')
                        shares.speed = []
                        shares.time = []
                        self.transitionTo(self.S1_WAITING)
                        
                
                
            else:
                # Uh-oh state (undefined sate)
                # Error handling
                pass
            
            self.next_time = utime.ticks_add(self.next_time, self.interval) # updating the "Scheduled" timestamp
            
    def transitionTo(self, newState):
        '''
        @brief Updates the state variable
        '''
        self.state = newState