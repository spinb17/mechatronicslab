'''
@file shares.py

@brief This file holds all the shared values for Lab_06

Code can be located at : https://bitbucket.org/spinb17/mechatronicslab/src/master/Lab_06/shares.py

@author Ben Spin
@date November 26 , 2020
'''

## Shared value of Kp
Kp = None

## List of time stamps 
time = []

## List of speeds
speed = []
