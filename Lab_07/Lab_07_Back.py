'''
@file Lab_07_Back.py

@brief This is the task that is in charge of writing to the UART and recording all the values that we collect

Code can be located at : https://bitbucket.org/spinb17/mechatronicslab/src/master/Lab_07/Lab_07_Back.py

@image html Lab_06_FSM_WritingTask.png

@author Ben Spin
@date November 26 , 2020
'''

import array
import utime
from pyb import USB_VCP
import shar

class TaskWriting:
    '''
    @brief TaskWriting is in charge of recording values and writing them the the UART
    '''
    ## State 0 Initializing
    S0_INIT                 = 0
    
    ## State 1 Waiting for Commands
    S1_GETTING_SPEED             = 1
    
    ## State 2 Waiting for Kp
    S2_GETTING_KP              =2
    
    ## State 2 Writing to the serial 
    S3_WRITING              = 3
    
    def __init__(self, Interval):
        '''
        @brief Creates a data collecting task object.
        @param interval An integer number of seconds between desired runs of the task
        '''
        ## Setting up UART for the Nucleo
        self.myuart = USB_VCP()
        
        ## Interval
        self.interval = int(Interval*1e6)
        
        ## Starting Time Stamp
        self.start_time = utime.ticks_us()
        
        ## Next Time for Task to run
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        ## State for running next iteration of the task.
        self.state = self.S0_INIT
        
        ## Counter is used to determine run times
        self.counter = 0
        
        
    def run(self):
        '''
        @brief Runs one iteration of the task
        '''
        
        self.curr_time = utime.ticks_us()    #updating the current timestamp
        
        ## checking if the timestamp has exceeded our "scheduled" timestamp
        if (utime.ticks_diff(self.curr_time, self.next_time) >= 0):
            if (self.state == self.S0_INIT):
                # Run State 0 Code
                self.transitionTo(self.S1_GETTING_SPEED)
                shar.ref = array.array('f', [])
                
                
            elif (self.state == self.S1_GETTING_SPEED):

                if self.myuart.any() == True:
                    self.counter += 1
                    read = self.myuart.readline().decode().strip()
                    # print(read)
                    # print(shar.ref)
                    # if read != ['e']:
                    if read != ['End']:
                        shar.ref.append(float(read))
                        # self.counter = 0
                    else:
                        self.transitionTo(self.S2_GETTING_KP)
                        print('counter works')
                        
            elif (self.state == self.S2_GETTING_KP):
                
                if self.myuart.any() == True:
                    kp = self.myuart.readline()
                    shar.Kp = kp
                    self.transitionTo(self.S3_WRITING)
                        
                
            elif (self.state == self.S3_WRITING):
                if shar.Kp == None:
                    length = len(shar.speed)
                    self.counter += 1

                    
                    if self.counter < length+1:
                        n = int(self.counter)
                        self.myuart.write('{:},{:}\r\n'.format(shar.speed[n-1],shar.time[n-1]))
                        
                    

                    else :
                        
                        self.myuart.write('End')
                        shar.speed = []
                        shar.time = []
                        self.transitionTo(self.S1_WAITING)
                        
                
                
            else:
                # Uh-oh state (undefined sate)
                # Error handling
                pass
            
            self.next_time = utime.ticks_add(self.next_time, self.interval) # updating the "Scheduled" timestamp
            
    def transitionTo(self, newState):
        '''
        @brief Updates the state variable
        '''
        self.state = newState