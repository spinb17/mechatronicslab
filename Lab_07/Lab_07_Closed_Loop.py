
'''
@file Lab_07_Closed_Loop.py

@brief Closed Loop holds the calculation for closed loop values 

The ClosedLoop class houses several important methods. 



A constructot __init__ that sets appropriate pins and timer use
Update() which is used to update quanity L at regular intervvals
get_Kp() which returns the current value of Kp
set_Kp() which sets a new value for Kp

You can find this code at : https://bitbucket.org/spinb17/mechatronicslab/src/master/Lab_07/Lab_07_Closed_Loop.py

@package encoder
The conder is a task and a class that interprets and take inputs

@author Ben Spin
@date November 26, 2020
'''

import shar

class ClosedLoop:
    
           
    '''
    
    @brief Defining modules to be run throught the Closed Loop Clas
    
    @details This class houses methods used to set and get our closed Loop gain which will be used to control the motor
    as well as an update method which will be used to return the acutation value based on the measured and reference values
    '''
    
    def __init__(self, Kp , Omega_Ref):
        '''
        @brief Does setup for the user, noting pins and timer to use
        @param Kp is the reference initial Kp value passed to ClosedLoop. This is delivered by the user
        @param Omega_Ref is used to set the target speed in RPMS for the controller to aim for
        '''
        self.Kp = Kp
        self.Omega_Ref = Omega_Ref

    def update(self,speed,n):
        '''
        @brief Takes an input speed and updates the value of L. When called consistently acts as our controller
        @param speed is encoder recorded speed
        @param n is the number within the array that we are opporating at
        '''

        
        L = (self.Kp) * (int(shar.ref[n-1])-int(speed))
        return L
    
    def get_Kp(self):
        '''
        @brief Returns a value Kp
        '''
        return self.Kp
    
    def set_Kp(self,Kp):
        '''
        @brief Sets the value of Kp in the controller to a specified value
        @param Kp is the enetered value of Kp to update with
        '''
        self.Kp = Kp
    
    
        