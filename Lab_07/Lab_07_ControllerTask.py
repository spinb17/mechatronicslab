'''
@file Lab_07_ControllerTask.py

@brief Controller tasks runs and constantly updates the running speed of the motor 




The purpose for this code is to be taking in recorded values from the encoder and the closed loops task
and adjusting the motor speed accordingly to compensate

You can find this code at : https://bitbucket.org/spinb17/mechatronicslab/src/master/Lab_07/Lab_07_ControllerTask.py

@image html Lab_06_FSM_ControllerTask.png

@author Ben Spin
@date November 26, 2020
'''

import array
import utime 
import shar

class ControllerTask:
    '''
    @brief Controller tasks runs and constantly updates the running speed of the motor

    @details Takes Motor, Encoder, and Closed loop inputs to contstatly update and evaulate the speed of the motor
    
    '''
    
    #Defining States
    
    ## State 0 Initializing Task
    S0_INIT = 0
    
    ## State 1 Waiting for User Imput
    S1_WAITING = 1
    
    ## Sate 2 Updating the information for the controller
    S2_UPDATING = 2
    
    ## State 3 Zeroing all information in anticipation of the creation of another graph
    S3_ZERO = 3
    
    def __init__ (self , MOTOR, ENCO , CLOOP , Interval):
        '''
        Creates a ControllerTask task object.
        
        @param MOTOR Imports the motor class
        @param ENCO  Imports the Encoder class
        @param CLOOP Imports the Closed Loop class
        @param Intercal Defines an Interval that the code will oeprate at 
        '''
        
        ## Encoder
        self.MOTOR= MOTOR
        
        
        ## Motor
        self.ENCO = ENCO
        
        ## CLOOP
        self.CLOOP = CLOOP
        
        ## Interval
        self.interval = int(Interval*1e6)
        
        ## Starting Time Stamp
        self.start_time = utime.ticks_us()
        
        ## Next Time for Task to run
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        ## State for running next iteration of the task.
        self.state = self.S0_INIT
        
        ## Time check for when Transition to S2 begins
        self.trans_time = 0
        
        ## Counter for runs through ref numbers
        self.ref_num = 0
        
        
    def run(self):
            
        '''
        @ brief Runs one iteration of the task
        '''
        
        ## updating the current timestamp
        self.curr_time = utime.ticks_us()    
        
        # checking if the timestamp has exceeded our "scheduled" timestamp
        if (utime.ticks_diff(self.curr_time, self.next_time) >= 0):
            
            if (self.state == self.S0_INIT):
                # Run State 0 Code
                
                self.transitionTo(self.S1_WAITING)
                
            
            elif (self.state == self.S1_WAITING):
                
                
                if shar.Kp != None :
                    self.CLOOP.set_Kp(shar.Kp)
                    self.trans_time = utime.ticks_us()
                    #making arrays
                    shar.time = array.array('f', [])
                    shar.speed = array.array('f', [])
                    shar.speed = array.array('f', [])
                    self.MOTOR.enable()
                    self.transitionTo(self.S2_UPDATING)
                    
            
            elif (self.state == self.S2_UPDATING):
                
                self.ref_num += 1
                self.ENCO.update()

                delta = self.ENCO.get_delta()
                pos1 = self.ENCO.get_position()
                
                pos = 94.6 * pos1 * (360/4000) 
                omega = 94.6*delta*(60/4000)/(self.interval/1e6) #Had to update this value with a fixed constant, this is an error somewhere in the encoder.
                L = self.CLOOP.update(omega,self.ref_num)
                L = int(L)
                self.MOTOR.set_duty(L)
                end_time = utime.ticks_diff(self.curr_time , self.trans_time)
                end_time_add = end_time
                # appending arrays
                shar.time.append(end_time_add)
                shar.speed.append(omega)
                shar.position.append(pos)
                if end_time >= 15e6:
                    self.transitionTo(self.S3_ZERO)
                    
                    
                        
            elif (self.state == self.S3_ZERO):
                shar.Kp = 0
                shar.ref = None
                self.CLOOP.set_Kp(shar.Kp)
                self.MOTOR.set_duty(0)
                self.ENCO.set_position()
                self.transitionTo(self.S1_WAITING)
                
            else:

                pass
        
            self.next_time = utime.ticks_add(self.next_time, self.interval) 
            
    def transitionTo(self, newState):
        '''
        @brief Updates the state variable
        '''
        self.state = newState