
'''
@file Lab_07_Front.py



@brief The is the front end for our lab 6 that runs the user interface functions

This takes an input Kp and returns a graph

It graphs the values and reurns an excel file populated with values for time and motor position

The three images below are the responses of the plot with increasing Kp , and therefore higher performance in the result
Figure 1 has a Kp of .015 , Figure 2 has a Kp of .02 , Figure 3 has a Kp of .025 That seems to work for us. Reference vaule for all is 1440

@image html Kp_lowest_point1.png
@image html Kp_mid_point3.png
@image html Kp_highest_point8.png

You can find this code at: https://bitbucket.org/spinb17/mechatronicslab/src/master/Lab_07/Lab_07_Front.py


@author Ben Spin
@date November 26, 2020

'''
import time
import serial
import matplotlib.pyplot as plt
import numpy as np

##serial object
ser = serial.Serial (port='Com5', baudrate=115200 , timeout=10)

# Blank lists for data. On the Nucleo you should probably use array.array() to
# better storage density. Make sure you pick the right data type when you init
# the array if you do go with array.array instead of a list.
tim = []
velocity = []
position = []

T = []
W=[]
P=[]
# Open the file. From here it is essentially identical to serial comms. There
# are better ways to do this, like using the 'with' statement, but using an
# object like this is closest to the serial code you've already used.
ref = open('reference.csv');

# Read data indefinitely. Loop will break out when the file is done.
#
# You will need to handle this slightly differently on the Nucleo or
# pre-process the provided data, because there are 15,001 rows of data in the
# CSV which will be too much to store in RAM on the Nucleo.
#
# Consider resampling the data, interpolating the data, or manipulating the 
# data in some fashion to limit the number of rows based on the 'interval' for
# your control task. If your controller runs every 20ms you do not need the
# data sampled at 1ms provided by the file.
#
while True:
    # Read a line of data. It should be in the format 't,v,x\n' but when the
    # file runs out of lines the lines will return as empty strings, i.e. ''
    line = ref.readline()
    
    # If the line is empty, there are no more rows so exit the loop
    if line == '':
        break
    
    # If the line is not empty, strip special characters, split on commas, and
    # then append each value to its list.
    else:
        (t,v,x) = line.strip().split(',');
        tim.append(float(t))
        velocity.append(float(v))
        position.append(float(x))

#Closing serial port
ref.close()

print('One seccond while we decode values...')

i = 0
while i<= 15001:
    ser.write(str(velocity[i]).encode('ascii'))
    i += 20

trans = 'e'
ser.write(trans.encode('ascii'))
time.sleep(18)



print ('Enter a value of Kp to begin data colletion')
print ('The simulation will run automatically for a while in order to generate the response graph, we appreciate your patience :)')

while True:
    try:
        start_data = (input('Enter a Kp Value: '))
        break
    except ValueError:
        print('Please enter an integer value')
        

t = [0]
motor_speed=[0]
value = 0
values = 0
run = 0
time.sleep(25)

while True:
    value = ser.readline()
    change = value.decode()
    values = change.strip().split(',')

    if values != ['End']:
        tim.append(float(values[1])*(1e-6))
        velocity.append(float(values[0]))
        position.append(float(values[2]))
        
    else:
        break

i = 0
length = len(t)
J = 0
while i < length:
    J += ((W[i] - velocity[i])**2)+((P[i] - position[i]**2))
    i += 1

J = J/i
print(str(J))

ser.close()

# To make the example complete, plot the data as a subplot.
plt.figure(1)

# Velocity first
plt.subplot(2,1,1)
plt.plot(time,velocity)
plt.ylabel('Velocity [RPM]')

# Then position
plt.subplot(2,1,2)
plt.plot(time,position)
plt.xlabel('Time [s]')
plt.ylabel('Position [deg]')
    
