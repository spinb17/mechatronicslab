'''
@file Lab_07_Main.py

@brief This file will run when a Kp value is entered and inits the other pieces of code. 
@details It is important that this be named main.py within the nucleo so that it will run automatically. This code will be running automatically 
but action will only occur when a Kp has been entered.

Code can be located at : https://bitbucket.org/spinb17/mechatronicslab/src/master/Lab_07/Lab_07_Main.py

@image html Lab_06_TaskDiagram.png

@author Ben Spin
@date November 26 , 2020

'''
import pyb
from Lab_07_ControllerTask import ControllerTask
from Lab_06_Encoder import Encoder
from Lab_07_Closed_Loop import ClosedLoop
from Lab_07_Back import TaskWriting
from Lab_06_Motor import MotorDriver


    
## Encoder for Lab 7
Encoder = Encoder(8, pyb.Pin.cpu.C6 , pyb.Pin.cpu.C7)

## Motor for Lab 7 
Motor = MotorDriver( pyb.Pin.cpu.A15  , pyb.Pin.cpu.B4 , pyb.Pin.cpu.B5 , 3 , 4 , 3 , 35  )

## Initial Kp value
Kp = 0

## Closed_Loop controller 
Closed_Loop = ClosedLoop(Kp , 1000)

## Defining controller task
task1 = ControllerTask( Motor , Encoder , Closed_Loop , 0.02)
## Defining writing Task
task2 = TaskWriting (0.02)

while True:
    task1.run()
    task2.run()