'''
@file shar.py

@brief This file holds all the shared values for Lab_06

Code can be located at : https://bitbucket.org/spinb17/mechatronicslab/src/master/Lab_07/shar.py

@author Ben Spin
@date November 26 , 2020
'''

## Gain value for controller
Kp = None

## Time stamps for graphing
time = None

## Omega_dot values for graphing
speed = None

## Omega_ref values for controller control
ref = None

## Position values for graphing
position = None